angular.module('starter.controllers', [])

.controller('AppCtrl',function($scope,$http){

    $scope.dominio = "http://localhost/sistemaLeilao/";

    $scope.searchImovelById = function($id){
      
     $http.get($scope.dominio+'imveis/find/'+$id)
        .success(function (data){
          $scope.imovel = data['Imvei'];
      });
    }
    
})

.controller('homeCtrl',function($scope,$http){

    $scope.imoveis = null;

    $scope.totalImov = 0;
    $scope.nrOcupado = 0;
    $scope.nrDesocupado = 0;


    //Metodo para ler todos imoveis
    function readAllImoveis() {
      
      $http.get($scope.dominio+'imveis/')
        .success(function (data){
          $scope.imoveis = data;

          for(i = 0; i < data.length;i++){
            if(data[i].Imvei.estado_ocupacao == 'Ocupado')
              $scope.nrOcupado++;
            else
              $scope.nrDesocupado++;
          }
      });       
    }

    $scope.getImovelByKey = function(){
      var key = document.getElementById("keyText").value;

      $http.get($scope.dominio+'imveis/index/'+key)
        .success(function (data){
          $scope.imoveis = data;
      });
    }

    readAllImoveis();

});